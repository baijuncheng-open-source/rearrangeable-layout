# RearrangeableLayout

a layout to re-arrange child components via dragging

## Integration method
Plan A：Local source integration, the user can be customized to modify
 1.The library folder copy to the project directory
 2.Modify the project Settings. Under the gradle, add reliance on library module as follows:

```
include ':entry', ':Library'
```

3.Dependence on the introduction of the import module under the project, to sample module, for example, you need to modify the sample under the module of the build. Gra

```

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation project(path: ':library')
    testImplementation 'junit:junit:4.13'
    ohosTestImplementation 'com.huawei.ohos.testkit:runner:1.0.0.100'
}
```

Plan B：Local har package integration.

1.Compile the project, will build the library folder directory generated har package copy to the project lib folder:

![输入图片说明](https://images.gitee.com/uploads/images/2021/0706/183605_7121a4fd_8230582.png "12.png")

2.Add the following code in the entry of gradle

```
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

Plan C：Maven remote warehouse

1. Add this in your root `build.gradle` file (**not** your module `build.gradle` file):
```xml
allprojects {
	repositories {
		...
		mavenCentral()
	}
}
```
2. The lib is available on Maven Central, you can find it with [Gradle, please]
```xml
dependencies {
    implementation ‘com.gitee.baijuncheng-open-source:rearrangeable-layout:1.0.0'
}
```

### Layout Usage

All the child components are draggable once the layout is added to an ability

### Screencast Demo
![输入图片说明](https://images.gitee.com/uploads/images/2021/0624/094239_2c0041c4_8230582.gif "tutieshi_480x1041_7s.gif")

```
<com.rajasharan.layout.RearrangeableLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.huawei.com/res/ohos-auto"
    ohos:id="$+id:rearrangeable_layout"
    ohos:height="match_parent"
    ohos:width="match_parent"
    app:outlineColor="$color:cyan"
    app:outlineWidth="2vp"
    app:selectionAlpha="0.5"
    app:selectionZoom="1.2"
    >

    <Text
        ohos:id="$+id:t4"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="$color:holo_orange_light"
        ohos:margin="15vp"
        ohos:text="Sample"
        ohos:text_size="15fp"
        />
</com.rajasharan.layout.RearrangeableLayout>
```

### Child Position Listener

Add a ChildPositionListener to the root layout to receive updates whenever any child component is dragged


```
@Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);

        root = (RearrangeableLayout) findViewById(R.id.rearrangeable_layout);
        root.setChildPositionListener(new RearrangeableLayout.ChildPositionListener() {
            @Override
            public void onChildMoved(View childView, Rect oldPosition, Rect newPosition) {
                Log.d(TAG, childView.toString());
                Log.d(TAG, oldPosition.toString() + " -> " + newPosition.toString());
            }
    });
}
```
