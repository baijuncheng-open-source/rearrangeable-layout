package com.rajasharan.demo;

import com.rajasharan.layout.RearrangeableLayout;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Rect;

import java.util.logging.Logger;

public class MainAbility extends Ability {
    private static final String TAG = "MainAbility";
    private RearrangeableLayout root;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);

        root = (RearrangeableLayout) findComponentById(ResourceTable.Id_rearrangeable_layout);
        root.setChildPositionListener(new RearrangeableLayout.ChildPositionListener() {
            @Override
            public void onChildMoved(Component childView, Rect oldPosition, Rect newPosition) {
                Logger.getLogger(TAG).info(childView.toString());
                Logger.getLogger(TAG).info(oldPosition.toString() + " -> " + newPosition.toString());
            }
        });
    }
}
