package com.rajasharan.layout;


import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.logging.Logger;

/**
 * Created by raja on 6/19/15.
 */
public class RearrangeableLayout extends ComponentContainer implements Component.EstimateSizeListener, ComponentContainer.ArrangeListener, Component.TouchEventListener {
    private static final String TAG = "RearrangeableLayout";

    private Point mStartTouch;
    private Component mSelectedChild;
    private float mSelectionZoom;

    /* callback to update clients whenever child is dragged */
    private ChildPositionListener mListener;

    /* used by ChildPositionListener callback */
    private Rect mChildStartRect;
    private Rect mChildEndRect;

    public RearrangeableLayout(Context context) {
        this(context, null);
    }

    public RearrangeableLayout(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public RearrangeableLayout(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        init(context, attrs);
    }

    private void init(Context context, AttrSet attrs) {
        setEstimateSizeListener(this);
        setArrangeListener(this);
        setTouchEventListener(this);

        mStartTouch = null;
        mSelectedChild = null;
        mListener = null;
        mChildStartRect = null;
        mChildEndRect = null;

        AttrUtil a = new AttrUtil(attrs);
        float strokeWidth = a.getDimensionValue("outlineWidth", AttrHelper.vp2px(2, context));
        Logger.getLogger(TAG).info("strokeWidth:" + strokeWidth);
        Color color = a.getColorValue("outlineColor", Color.GRAY);
        Logger.getLogger(TAG).info("color:" + color.getValue());
        float alpha = a.getFloatValue("selectionAlpha", 0.5f);
        Logger.getLogger(TAG).info("alpha:" + alpha);
        mSelectionZoom = a.getFloatValue("selectionZoom", 1.2f);
        Logger.getLogger(TAG).info("mSelectionZoom:" + mSelectionZoom);
    }

    @Override
    public LayoutConfig createLayoutConfig(Context context, AttrSet attrSet) {
        return new LayoutParams(getContext(), attrSet);
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int width = EstimateSpec.getSize(widthMeasureSpec);
        int height = EstimateSpec.getSize(heightMeasureSpec);
        width = Math.max(width, getMinWidth());
        height = Math.max(height, getMinHeight());
        Logger.getLogger(TAG).info("width:" + width + " height:" + height);

        for (int i = 0; i < getChildCount(); i++) {
            Component view = getComponentAt(i);
            LayoutParams mp = (LayoutParams) view.getLayoutConfig();
            Logger.getLogger(TAG).info("mp" + i + ": MarginLeft" + mp.getMarginLeft() + " MarginRight:" + mp.getMarginRight());
            Logger.getLogger(TAG).info("mp" + i + ": MarginTop" + mp.getMarginTop() + " MarginBottom:" + mp.getMarginBottom());
            view.estimateSize(EstimateSpec.getSizeWithMode(
                    width - mp.getMarginLeft() - mp.getMarginRight(), EstimateSpec.NOT_EXCEED),
                    EstimateSpec.getSizeWithMode(height - mp.getMarginTop() - mp.getMarginBottom(),
                            EstimateSpec.NOT_EXCEED));
            Logger.getLogger(TAG).info("mp" + i + " width:" + view.getEstimatedWidth() + " height:" + view.getEstimatedHeight());

        }
        setEstimatedSize(width, height);
        return true;
    }

    @Override
    public boolean onArrange(int l, int t, int r, int b) {
        if (mSelectedChild == null) {
            doInitialLayout(l, t, r, b, getChildCount());
        }
        return true;
    }

    private void doInitialLayout(int l, int t, int r, int b, int count) {
        Logger.getLogger(TAG).severe("left:" + l + " top:" + t + " right:" + r + " bottom:" + b + " count:" + count);
        int currentLeft = l;
        int currentTop = t;
        int prevChildBottom = -1;
        for (int i = 0; i < count; i++) {
            Component view = getComponentAt(i);
            LayoutParams mp = (LayoutParams) view.getLayoutConfig();
            int width = view.getEstimatedWidth();
            int height = view.getEstimatedHeight();
            int left, top, right, bottom;
            Logger.getLogger(TAG).info(i + " width:" + width + " height:" + height);

            if (!mp.moved) {
                if (currentTop + height > b || l + width > r) {
                    new ToastDialog(getContext()).setText("Couldn't fit a child View, skipping it").show();
                    Logger.getLogger(TAG).info("Couldn't fit a child View, skipping it");
                    continue;
                }
                if (currentLeft + width > r) {
                    left = l + mp.getMarginLeft();
                    currentTop = prevChildBottom;
                } else {
                    left = currentLeft + mp.getMarginTop();
                }
                top = currentTop + mp.getMarginTop();
                right = left + width;
                bottom = top + height;
                mp.left = left;
                mp.top = top;
                Logger.getLogger(TAG).severe(i + " left:" + left + " top:" + top + " right:" + right + " bottom:" + bottom);
                view.arrange(left, top, width, height);
                Logger.getLogger(TAG).info(i + " width:" + view.getWidth() + " height:" + view.getHeight());
                currentLeft = right + mp.getMarginRight();
                prevChildBottom = bottom + mp.getMarginBottom();
            } else if (mp.moved && view != mSelectedChild) {
                int x1 = Math.round(mp.left);
                int y1 = Math.round(mp.top);
                int x2 = Math.round(mp.left) + width;
                int y2 = Math.round(mp.top) + height;
                Logger.getLogger(TAG).info(i + " x1:" + x1 + " y1:" + y1 + " x2:" + x2 + " y2:" + y2);
                view.arrange(x1, y1, width, height);
            }
        }
    }

    /**
     * this method can be used to force layout on a child
     * to recalculate its hit-rect,
     * otherwise outline border of the selected child is
     * drawn at the old position
     */
    private void layoutSelectedChild(LayoutParams lp) {
        int l = Math.round(lp.left);
        int t = Math.round(lp.top);
        int width = mSelectedChild.getEstimatedWidth();
        int height = mSelectedChild.getEstimatedHeight();
        lp.moved = true;
        Logger.getLogger(TAG).severe("layoutSelectedChild left:" + l + " top:" + t + " width:" + width + " height:" + height);
        mSelectedChild.arrange(l, t, width, height);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        float x = event.getPointerPosition(0).getX();
        float y = event.getPointerPosition(0).getY();

        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                prepareTouch(x, y);
                break;
            case TouchEvent.POINT_MOVE:
                if (mSelectedChild != null && mStartTouch != null) {
                    LayoutParams lp = (LayoutParams) mSelectedChild.getLayoutConfig();
                    float dx = x - mStartTouch.getPointX();
                    float dy = y - mStartTouch.getPointY();

                    lp.left = lp.initial.getPointX() + dx;
                    if (lp.left < 0.0f) {
                        lp.left = 0.0f;
                    }

                    lp.top = lp.initial.getPointY() + dy;
                    if (lp.top < 0.0f) {
                        lp.top = 0.0f;
                    }

                    /* layout child otherwise hit-rect is not recalculated */
                    layoutSelectedChild(lp);
                    invalidate();
                }
                break;
            case TouchEvent.CANCEL:
            case TouchEvent.PRIMARY_POINT_UP:
            default:
                if (mSelectedChild != null) {
                    if (mListener != null && mChildStartRect != null) {
                        mChildEndRect = mSelectedChild.getComponentPosition();
                        mListener.onChildMoved(mSelectedChild, mChildStartRect, mChildEndRect);
                    }

                    mSelectedChild.setVisibility(Component.VISIBLE);
                    mSelectedChild = null;
                }
                break;
        }
        return true;
    }

    private void prepareTouch(float x, float y) {
        mStartTouch = null;
        mSelectedChild = findChildViewInsideTouch(Math.round(x), Math.round(y));
        if (mSelectedChild != null) {
            moveChildToFront(mSelectedChild);
            LayoutParams lp = (LayoutParams) mSelectedChild.getLayoutConfig();
            lp.initial = new Point(lp.left, lp.top);
            mStartTouch = new Point(x, y);
            if (mChildStartRect == null) {
                mChildStartRect = mSelectedChild.getComponentPosition();
            }
        }
    }

    /**
     * Search by hightest index to lowest so that the
     * most recently touched child is found first
     *
     * @return selectedChild
     */
    private Component findChildViewInsideTouch(int x, int y) {
        for (int i = getChildCount() - 1; i >= 0; i--) {
            Component view = getComponentAt(i);
            Rect rect = view.getComponentPosition();
            Logger.getLogger(TAG).severe("rect:" + rect);
            if (rect.isInclude(x, y)) {
                mChildStartRect = rect;
                return view;
            }
        }
        return null;
    }

    public static class LayoutParams extends LayoutConfig {
        float left;
        float top;
        Point initial;
        boolean moved;

        public LayoutParams(Context c, AttrSet attrs) {
            super(c, attrs);
            left = -1.0f;
            top = -1.0f;
            initial = new Point(0.0f, 0.0f);
            moved = false;
        }
    }

    /**
     * set ChildPositionListener to receive updates whenever child is moved
     *
     * @param listener
     */
    public void setChildPositionListener(ChildPositionListener listener) {
        mListener = listener;
    }

    public interface ChildPositionListener {
        /**
         * this callback is invoked whenever child is moved
         *
         * @param childView the current child view that was dragged
         * @param oldPosition the original position from where child was dragged
         * @param newPosition the new position where child is currently laid
         */
        void onChildMoved(Component childView, Rect oldPosition, Rect newPosition);
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder(128);
        out.append(TAG);
        out.append(" mSelectedChild: ");
        if (mSelectedChild != null) {
            out.append(this.mSelectedChild.toString());
        }
        return out.toString();
    }
}
